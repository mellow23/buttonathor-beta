// INITIALIZATION
window.onload = function() {
    CanvasRenderingContext2D.prototype.roundRect = 

    function(x, y, width, height, radius, fill, stroke) {
        if (typeof stroke == "undefined" ) {
            stroke = false
        }
        if (typeof radius === "undefined") {
            radius = 0
        }
        this.beginPath()
        this.moveTo(x + radius, y)
        this.lineTo(x + width - radius, y)
        this.quadraticCurveTo(x + width, y, x + width, y + radius)
        this.lineTo(x + width, y + height - radius)
        this.quadraticCurveTo(x + width, y + height, x + width - radius, y + height)
        this.lineTo(x + radius, y + height)
        this.quadraticCurveTo(x, y + height, x, y + height - radius)
        this.lineTo(x, y + radius)
        this.quadraticCurveTo(x, y, x + radius, y)
        this.closePath()
        if (stroke) {
            this.stroke()
        }
        if (fill) {
            this.fill()
        }        
    }
    //START
    window.requestAnimationFrame(draw)
}


// MAIN LOOP
function draw() {
    // console.log('draw function is running')
    // VARIABLES
    let bgColor = document.getElementById('bgColor').value

    let w = parseInt(document.getElementById('widthInput').value)
    let h = parseInt(document.getElementById('heightInput').value)

    let borderRadius = document.getElementById('borderRadius').value
    
    let offsetTop = parseInt(document.getElementById('offsetTopInput').value)
    let offsetBottom = parseInt(document.getElementById('offsetBottomInput').value)
    let offsetLeft = parseInt(document.getElementById('offsetLeftInput').value)
    let offsetRight = parseInt(document.getElementById('offsetRightInput').value)
    let exportAreaSize = document.getElementById('exportAreaSize')
    let canvasBorderCheckbox = document.getElementById('canvasBorderCheckbox')

    let text = document.getElementById('textInput').value
    let textX = parseInt(document.getElementById('textX').value)
    let textY = parseInt(document.getElementById('textY').value)
    let fontSpecs = document.getElementById('fontSpecs').value
    let fontColor = document.getElementById('fontColor').value
    let textWidthP = document.getElementById('textWidthP')
    let centerTextButton = document.getElementById('centerTextButton')

    let text2 = document.getElementById('text2Input').value
    let text2X = parseInt(document.getElementById('text2X').value)
    let text2Y = parseInt(document.getElementById('text2Y').value)
    let font2Specs = document.getElementById('font2Specs').value
    let font2Color = document.getElementById('font2Color').value
    let text2WidthP = document.getElementById('text2WidthP')
    let centerText2Button = document.getElementById('centerText2Button')
    


    // SOME INPUT VALIDATION
    if(document.getElementById('offsetTopInput').value.length == 0) {
        document.getElementById('offsetTopInput').value = 0
    }

    if(document.getElementById('offsetBottomInput').value.length == 0) {
        document.getElementById('offsetBottomInput').value = 0
    }

    if(document.getElementById('offsetLeftInput').value.length == 0) {
        document.getElementById('offsetLeftInput').value = 0
    }

    if(document.getElementById('offsetRightInput').value.length == 0) {
        document.getElementById('offsetRightInput').value = 0
    }

    if(document.getElementById('borderRadius').value < 0) {
        document.getElementById('borderRadius').value = 0
    }    

    // CANVAS SETTINGS    
    let canvas = document.getElementById('canvas')
    let c = canvas.getContext('2d')

    canvas.width = w + offsetLeft + offsetRight
    canvas.height = h + offsetTop + offsetBottom

    c.msImageSmoothingEnabled = false
    c.imageSmoothingEnabled = false
    
     // EVENT LISTENERS    
    document.getElementById('fileInput').addEventListener('change', readImage)
    document.getElementById('canvasBorderCheckbox').addEventListener('change',setCanvasBorder)
    centerTextButton.addEventListener('click', function() {
        document.getElementById('textX').value = w/2
        document.getElementById('textY').value = h/2
    })
    centerText2Button.addEventListener('click', function() {
        document.getElementById('text2X').value = w/2
        document.getElementById('text2Y').value = h/2
    })
    
    // DISPLAYING EXPORT AREA SIZE
    exportAreaSize.innerHTML = canvas.width + " x " + canvas.height

    // GENERATING RECTANGLE
    c.fillStyle = bgColor
    c.translate(offsetLeft, offsetTop)
    c.roundRect(0,0,w,h,borderRadius,bgColor)
    c.translate(-offsetLeft, -offsetTop)
    
    // GENERATING TEXT 
    c.fillStyle = fontColor
    c.font = fontSpecs
    c.textBaseline = 'middle'
    c.textAlign = 'center'
    c.fillText(text, textX + offsetLeft, textY + offsetTop)
    textWidthP.innerHTML = parseInt(c.measureText(text).width) + " px"

    // GENERATING TEXT_2
    c.fillStyle = font2Color
    c.font = font2Specs
    c.textBaseline = 'middle'
    c.textAlign = 'center'
    c.fillText(text2, text2X + offsetLeft, text2Y + offsetTop)
    text2WidthP.innerHTML = parseInt(c.measureText(text2).width) + " px"

    // SET LOOP
    window.requestAnimationFrame(draw)
}


// OTHER FUNCTIONS
function readImage(e){
    let reader = new FileReader()
    reader.onload = function(event){
        let img = new Image()
        img.onload = function(){
            let c = document.getElementById('canvas').getContext('2d')
            let posX = parseInt(document.getElementById('imageX').value)
            let posY = parseInt(document.getElementById('imageY').value)
            c.drawImage(img,posX,posY)
            window.requestAnimationFrame(img.onload)
        }
        img.src = event.target.result
    }
    reader.readAsDataURL(e.target.files[0])
}

function setCanvasBorder() {
    var canvas = document.getElementById('canvas')
    canvas.classList.toggle('canvasBorder')
}

