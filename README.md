### PNG button generator  :sparkles:

Currently included fonts:   

* "Open Sans" 		+ light, semibold, bold, extrabold, italic
* "Exo2" 			+ light, semibold, bold, extrabold, italic
* "Inconsolata"		+ bold

Format of input for specifying font style is: "size name style" without quotes.   
Styles you can select from, are: italic, light, bold, semibold, extrabold   
Regular font style is selected, when style parameter is not specified. Using regular as style paremeter wont work.   

#####Examples of valid inputs:   

24px open sans italic   
28px inConSolAta   
16px exo2 bOlD   

**On windows machines you need to specify your style first, ie: bold 24px inconsolata.**

*Note: Font names __are space-sensitive__. Be careful to type spaces in font names correctly.   
They __are NOT case-sensitive__, so you dont need to care about upper and lower cases.*   

You can set transparent margin around the button by using "Offset" values.   
You can show and hide border of export area by clicking on "Show export area" checkbox.   

To save your button, just right-click on it and "save image as.."   

*Tested in latest Chrome.*   
*File "search-icon.png" is included as sample file for testing purposes*

V pripade problemov s nenacitavanim fontov, skuste:
a) spusit local server v priecinku buttonathor-beta , napr. python -m SimpleHTTPServer 8000 do terminalu
b) nainstalovat fonty v adresari do systemu a restartovat chrome